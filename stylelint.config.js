export default {
  extends: ['stylelint-config-recess-order', 'stylelint-config-standard-vue'],

  rules: {
    // Give weird errors
    'no-descending-specificity': null
  },

  ignoreFiles: ['**/*.html']
}
