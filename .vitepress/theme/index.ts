// https://vitepress.dev/guide/custom-theme
import DefaultTheme from 'vitepress/theme'

import Layout from './Layout.vue'
import './style.css'
// Initialize Firebase
import './useFirebase'

export default {
  ...DefaultTheme,
  // https://vitepress.dev/guide/extending-default-theme#layout-slots
  Layout
}
