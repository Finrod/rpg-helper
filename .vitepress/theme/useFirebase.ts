import { initializeApp } from 'firebase/app'
import { getAuth, onAuthStateChanged } from 'firebase/auth'
import { doc, getFirestore } from 'firebase/firestore'
import { computed, ref } from 'vue'

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyCjwwSroaJUDBVL9aI3k3M_uUC0dMc6FD0',
  authDomain: 'tools-26d74.firebaseapp.com',
  projectId: 'tools-26d74',
  storageBucket: 'tools-26d74.appspot.com',
  messagingSenderId: '136755651778',
  appId: '1:136755651778:web:3b8f4ea837f0ce8a2e475b'
}
export const email = 'sylvain.fave@gmail.com'

// Initialize Firebase
export const firebaseApp = initializeApp(firebaseConfig)
export const auth = getAuth()
// Database, export document for each characters
const db = getFirestore(firebaseApp)
export const docMirielle = doc(db, 'rpg-helper', 'mirielle')
export const docDragann = doc(db, 'rpg-helper', 'dragann')
export const docCacophonir = doc(db, 'rpg-helper', 'cacophonir')

// Dynamic ref to connected user, to be used with `useFirebase()`
const user = ref()

// Update user on change
onAuthStateChanged(auth, (connectedUser) => {
  if (connectedUser) {
    // User is signed in, see docs for a list of available properties
    // https://firebase.google.com/docs/reference/js/auth.user
    user.value = connectedUser
  } else {
    user.value = null
  }
})

export function useFirebaseUser() {
  const isAuthenticated = computed(() => !!user.value?.uid)

  return {
    isAuthenticated,
    user
  }
}
