import { URL, fileURLToPath } from 'node:url'
import { ViteImageOptimizer } from 'vite-plugin-image-optimizer'
import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  // Site config
  title: 'RPG Helper',
  description: 'Docs et stats pour mes parties de jeu de rôle',
  head: [['link', { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/rpg-helper/favicon.png' }]],

  appearance: 'force-dark',

  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    logo: 'logo.png',

    darkModeSwitchLabel: 'Thème',
    outline: {
      label: 'Sur cette page'
    },

    nav: [
      {
        text: 'Anathazerïn',
        items: [
          { text: 'Mirielle & Enya', link: '/cof-anathazerin/mirielle-enya' },
          { text: 'Histoire', link: '/cof-anathazerin/story' },
          { text: 'Macros', link: '/cof-anathazerin/macros' },
          { text: 'Infos utiles', link: '/cof-anathazerin/notes' }
        ]
      },
      {
        text: 'Descente en Averne',
        items: [
          { text: 'Dragann', link: '/dd5-baldur-gate/dragann' },
          { text: 'Histoire', link: '/dd5-baldur-gate/story' },
          { text: 'Infos utiles', link: '/dd5-baldur-gate/notes' }
        ]
      },
      {
        text: 'Odyssée des Seigneurs Dragons',
        items: [
          { text: 'Cacophonir', link: '/dd5-seigneurs-dragons/cacophonir' },
          { text: 'Histoire', link: '/dd5-seigneurs-dragons/story' },
          { text: 'Infos utiles', link: '/dd5-seigneurs-dragons/notes' }
        ]
      }
    ]
  },

  // Dev
  vite: {
    resolve: {
      alias: [
        // use @ for components folder instead of relative paths
        { find: '@', replacement: fileURLToPath(new URL('./theme/components', import.meta.url)) }
      ]
    },

    plugins: [ViteImageOptimizer()]
  },

  // Build
  srcDir: './src',
  outDir: './public',

  // Publish
  base: '/rpg-helper'
})
