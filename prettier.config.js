export default {
  printWidth: 120,
  semi: false,
  singleQuote: true,
  trailingComma: 'none',

  importOrder: ['^components/(.*)$', '^[./]'],
  importOrderSeparation: true,
  importOrderSortSpecifiers: true,

  plugins: ['@trivago/prettier-plugin-sort-imports']
}
