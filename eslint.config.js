import js from '@eslint/js'
import eslintConfigPrettier from 'eslint-config-prettier'
import pluginVue from 'eslint-plugin-vue'
import tseslint from 'typescript-eslint';

export default [
  // Plugins and premade configurations
  js.configs.recommended,
  ...tseslint.configs.recommended,
  ...pluginVue.configs['flat/recommended'],
  eslintConfigPrettier,

  // Ignore must be alone in its own configuration object to be global
  // https://github.com/eslint/eslint/discussions/18304
  {
    ignores: ['dist/', 'public/', '/build', '.vitepress/cache']
  },

  // Custom configurations
  {
    files: ['**/*.vue', '**/*.js', '**/*.cjs'],

    rules: {
      'vue/multi-word-component-names': 'off',

      // Custom rules from uncategorized category
      'vue/component-name-in-template-casing': ['error', 'PascalCase', { registeredComponentsOnly: false }],
      'vue/match-component-file-name': [
        'error',
        {
          extensions: ['js', 'jsx', 'ts', 'tsx', 'vue'],
          shouldMatchCase: true
        }
      ],
      'sort-imports': [
        'error',
        {
          ignoreDeclarationSort: true,
          allowSeparatedGroups: true
        }
      ]
    }
  }
]
