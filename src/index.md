---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

title: RPG Helper
titleTemplate: Docs et stats pour mes parties de jeu de rôle

hero:
  name: "RPG Helper"
  tagline: "Docs et stats pour mes parties de jeu de rôle"
  image:
    src: /logo.png
    alt: VitePress
features:
  - title: Anathazerïn
    icon:
      src: /token-mirielle.png
    details: |
      <h3>Mirielle - Druide Elfe</h3><br/>
      Dans le village paisible de Clairval, la rumeur d’une grande guerre enfle. Quand le Baron Rodrick cherche des âmes courageuses pour aller porter des vivres au fort posté à la frontière Nord du territoire, les volontaires sont loin de se douter qu’ils s’embarquent pour l’aventure de leur vie !
      <h4>Aventure terminée</h4>
    link: /cof-anathazerin/mirielle-enya

  - title: Descente en Averne
    icon:
      src: /token-dragann.png
    details: |
      <h3>Dragann - Paladin Nain</h3><br/>
      La Porte de Baldur, une ville où le meurtre, l’ambition et la corruption règnent en maîtres. À peine commencez-vous votre carrière d'aventurier que vous voilà déjà entraîné dans un complot s'étendant depuis depuis les ombres de la Porte de Baldur jusqu'à la ligne de front de la Guerre de Sang !
      <h4>Aventure en cours...</h4>
    link: /dd5-baldur-gate/dragann

  - title: Odyssée des Seigneurs Dragons
    icon:
      src: /token-cacophonir.png
    details: |
      <h3>Cacophonir - Barde Demi-Elfe</h3><br/>
      À l’aube des temps, la guerre entre les dieux et les titans changea le paysage de Thyléa. Plus tard, les premiers mortels apparurent, portés par des navires et des dragons. Les Seigneurs Dragons renversèrent les titans et forgèrent le Serment de Paix. Mais le pouvoir du traité faiblit et, à présent, les titans réclament vengeance.
      <h4>Aventure en cours...</h4>
    link: /dd5-seigneurs-dragons/cacophonir
---
