# Macros

Macros du système COF : https://github.com/ZigmundKreud/cof/blob/master/module/system/macros.js

## Exemple de macro JS basé sur un item

```js
// Exemple de macro JS basé sur un item (capacité ou équipement)
// N'oubliez pas de sélectionner "Scénario" comme type de la macro
// Editez juste les valeurs des variables au début, pas besoin d'éditer le code après

// Id de l'item utilisé
// Pour avoir l'id, glisser/déposer l'item dans la barre de macro, ce sera le premier argument
// de la fonction "rollItemMacro"
const itemId = "qsgOWN1wQmLtX8oR"
// Nom de l'item
const itemName = "Arc long affûté"
// Type de l'item ("item" ou "capacity")
const itemType = "item"
// Bonus d'attaque
const bonus = 0
// malus d'attaque
const malus = 0
// Bonus de dégats
// peut-être un chiffre : 2 par exemple
// ou un lancer de dés : "1d6" par exemple (attention aux quotes !)
const dmgBonus = "1d6"
// Uniquement jet de dégats (true ou false)
let onlyDamage = false;
// Label personnalisé pour la macro
const customLabel = "Flèche sanglante";
// Description personnalisée de compétence, s'affiche au jet d'attaque
const skillDescription = "La flèche produit un effet de saignement qui inflige à la victime +1d6 DM à chaque tour suivant jusqu’à ce que la cible réussisse un test de CON difficulté 16 (12 + Mod. de DEX) ou qu’elle soit soignée. On ne peut cumuler plusieurs effets de saignement.";
// Description personnalisée des dégats, s'affiche au jet de dégats
const dmgDescription = "";
// Affichage de la fenêtre de modification des jets (true: affiche la fenêtre, false: lance directement la macro)
const withDialog = true;


/** Pas besoin de toucher quoi que ce soit à partir d'ici :) **/

// Lancer la macro avec la touche shift appuyée ne fait que le jet de dégats
if (event && event.shiftKey) onlyDamage = true;

// Lancement de la fonction de macro
// Code source : https://github.com/ZigmundKreud/cof/blob/master/module/system/macros.js#L197
game.cof.macros.rollItemMacro(itemId, itemName, itemType, bonus, malus, dmgBonus, onlyDamage, customLabel, skillDescription, dmgDescription, withDialog);
```

## Récupération des statistiques du personnage

```js
// Exemple de macro JS pour récupérer les stats du personnage sélectionné
const selectedActor = game.cof.macros.getSpeakersActor();

/**
 * Les stats sont dans: selectedActor.system.stats
 * Clés de stats: "str" | "dex" | "con" | "int" | "wis" | "cha"
 * Object stats:
 * {
 *   "key": "cha",
 *   "label": "COF.stats.cha.label",
 *   "abbrev": "COF.stats.cha.abbrev",
 *   "base": 14,
 *   "racial": 0,
 *   "bonus": 0,
 *   "mod": 2,
 *   "value": 14,
 *   "tmpmod": null,
 *   "superior": false,
 *   "skillbonus": 0,
 *   "skillmalus": 0
 * }
 */
```

## Equiper / déséquiper un item

https://github.com/foundry-vtt-community/macros/blob/main/misc/equip_unequip_shield.js
