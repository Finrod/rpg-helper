---
title: Mirielle & Enya
layout: page
prev: false
next: false
---

<script setup>
import stats from './stats.json'
import eventBus from '@/../eventBus'

import Grid from '@/Grid.vue'
import Stats from '@/Stats.vue'
import Skills from '@/Skills.vue'
import GridBox from '@/GridBox.vue'
import GridCol from '@/GridCol.vue'
import Equipment from '@/Equipment.vue'
import CapacityCounter from '@/CapacityCounter.vue'
import BtnStop from '@/BtnStop.vue'

// Send an event to reset all counters
function resetCounters() {
  eventBus.emit('reset-counters')
}
</script>

# Mirielle & Enya

<Grid>
  <GridCol :size="2">

  ## Capacités

  **Langues** : [Elfique](https://www.co-drs.org/fr/jeu/races/elfe-sylvain), Commun, Elfe sombre (proche de l'Elfe noir)

  **Lumière des étoiles** : L'obscurité de la nuit sous la lumière des étoiles n’est que pénombre, où seuls les petits détails lui échappent

  **Athlétisme** : +3

  </GridCol>

  <GridCol :size="1">
    <Skills :stats="stats" />
  </GridCol>

  <GridCol :size="1">

  ## Sorts et états temporaires <BtnStop class="cc-action cc-action-global" @click="resetCounters" />

  <CapacityCounter char="mirielle" id="bark-skin" label="Peau d'écorce" :start="stats.mods.sag + 5" />
  <CapacityCounter char="mirielle" id="plant-prison" label="Prison végétale" :start="stats.mods.sag + 5" />
  <CapacityCounter char="mirielle" id="animated-tree" label="Arbre animé" :start="stats.level" />

  </GridCol>

  <GridCol :size="1">
    <Stats :stats="stats" />
  </GridCol>
</Grid>

## Voies

<Grid>
  <GridBox type="nature">

  ### Voie de la nature

  #### Maître de la survie

  Le Druide obtient un bonus de **+2 par rang dans la voie** à tous les tests basés sur la **survie en milieu naturel** (survie, vigilance, discrétion, etc.).

  #### Marche sylvestre

  Non seulement le Druide ne subit **aucune pénalité de déplacement en terrain difficile** (neige, boue, broussailles, pente abrupte, etc.) mais en plus, il obtient un bonus de **+2 en ATT et en DEF** lors d’un combat dans ces conditions.

  </GridBox>
  <GridBox type="panther">

  ### Voie du fauve

  #### Vitesse du félin

  Le Druide gagne **+1 par rang dans la voie en Initiative et aux tests de course, d’escalade ou de saut**.

  #### Panthère

  Le Druide apprivoise une **panthère** (ou un puma) qui lui obéit au doigt et à l’œil.<br />
  Panthère : FOR +2, DEX +4*, CON +2, INT -3, SAG +2*, CHA -2, Init 18, DEF 16, PV [niveau x 4], Attaque au contact [niveau du druide], DM 1d6+2

  #### Attaque bondissante (L)

  Le Druide parcourt jusqu’à 30 m et bénéficie d’un bonus de **+5 au test d’attaque et de +1d6 aux DM** contre sa cible. Il doit se déplacer d’au minimum 5 m en ligne droite pour faire cette attaque, qui ne peut par ailleurs être réalisée qu’à son premier tour du combat.

  #### Grand félin

  La panthère devient un **animal fabuleux**, ou est remplacée par un félin plus grand (tigre, lion).<br />
  Lorsque le Druide atteint le niveau 8, les DM passent à 2d6+5.<br />
  Au niveau 12, le félin peut utiliser Attaque bondissante. Le Druide peut également communiquer avec son félin par télépathie, et le guérir à distance en dépensant ses propres PV (-1 PV au Druide par PV octroyé au félin).<br />
  Panthère : FOR +5, DEX +4*, CON +5, INT -3, SAG +2*, CHA -2, DEF 18, DM 1d6+5

  </GridBox>
  <GridBox type="vegetal">

  ### Voie des végétaux

  #### Peau d’écorce (L) *

  La peau du Druide prend la consistance de l’écorce. Il gagne **+1 en DEF** par rang dans la voie pendant [5 + Mod. SAG] tours.

  #### Prison végétale (L) *

  Le Druide peut commander à la végétation de pousser et bloquer ses ennemis (mais pas ses alliés) dans une zone de 10 m de diamètre (portée 20 m) pendant [5 + Mod. SAG] tours. Entravées, les cibles subissent un malus de **-2 en ATT et en DEF, et ne peuvent pas se déplacer**. Chaque tour, une créature peut se libérer avec un test de FOR difficulté [10 + Mod. SAG].

  #### Animation d’un arbre (L) *

  Une fois par combat, le Druide peut **animer un arbre en le touchant**. Il combat pendant [niveau du druide] tours.<br />

  #### Gland de pouvoir (L) *

  Une fois par combat, le Druide peut lancer un gland sur une cible (portée 10 m). En cas d'attaque magique réussie, **la victime se transforme en statue de bois** pendant [2d6 + Mod. SAG] tours. Sous cette forme **elle ne peut agir et ne ressent rien**. Sa DEF passe à 10 mais elle gagne une réduction des DM de 10. Le sort s'achève dès que la cible perd plus de 10 PV.

  #### Porte végétale (L) *

  Une fois par jour, le Druide peut pénétrer dans le tronc d'un gros arbre et sortir de celui d'un autre arbre appartenant à la même forêt et situé à une distance maximum de [Mod. SAG] x 10 km.

  </GridBox>
  <GridBox type="elf">

  ### Voie de l'Elfe Sylvain

  #### Grâce elfique

  L’elfe gagne un bonus de **+5 en Initiative** et à tous les tests de **DEX**.

  #### Enfant de la forêt

  Le joueur choisit une capacité de rang 1 de n’importe quelle voie de Druide ou de Rôdeur. Il peut utiliser cette capacité en armure sans pénalité. Au rang 4 de la voie, il pourra choisir une capacité supplémentaire de rang 2.

  #### Archer émérite

  Lorsqu’il utilise un arc, l’elfe sylvain obtient une réussite **critique sur un résultat de 19-20** au d20. Il sait utiliser les arcs quel que soit son profil.

  #### Flèche sanglante (L)

  L’elfe fait une attaque à distance qui provoque une hémorragie. En plus des DM normaux, la flèche produit un effet de saignement qui inflige à la victime **+1d6 DM à chaque tour** suivant jusqu’à ce que la cible réussisse un test de CON difficulté [12 + Mod. DEX] ou qu’elle soit soignée. On ne peut cumuler plusieurs effets de saignement.

  #### Supériorité Elfique

  L’elfe augmente ses valeurs de DEX et SAG de +2.

  </GridBox>
  <GridBox type="protector">

  ### Voie de l'Elfe Sylvain - Enfant de la forêt

  #### Voie de l'archer (Rodeur) - Sens affûtés

  Pour **chaque rang dans cette Voie, le Rôdeur gagne un bonus de +2 à tous ses tests de SAG destinés à simuler la perception** (vue, ouïe, vigilance, etc.). De plus, il ajoute son **Mod. SAG aux dégâts qu’il inflige à l’arc et à son Initiative**.

  #### Voie du Protecteur (Druide) - Forêt vivante (L) *

  La forêt s’éveille dans un rayon d’1 km par rang et devient une alliée du Druide pendant les 12 prochaines heures. Dans ce périmètre, les ennemis du Druide sont désorientés et gênés par les branches et les racines. Ils **divisent leur déplacement par deux et subissent une pénalité de -5 en Initiative et à tous les tests de survie, d’orientation, de perception ou de discrétion**. Si deux Druides essayent d’influencer la forêt, c’est celui dont le niveau est le plus élevé qui l’emporte.
  </GridBox>
</Grid>

## Equipement

<Grid>
  <Equipment type="bow">

  Arc long affuté +2
  *Chances de réussite critique +1 et ajoute +1d6 aux dégats critiques*

  </Equipment>

  <Equipment type="rapier">

  Rapière de Haute Dextérité +2
  *Ajoute +2 au mod. DEX*

  </Equipment>

  <Equipment type="helmet">

  Serre-tête de sagesse +1
  *Ajoute +1 au mod. SAG*

  </Equipment>

  <Equipment type="talisman">

  Talisman anti-poison

  </Equipment>

  <Equipment type="armor">

  Armure sur mesure de qualité DEF+3

  </Equipment>

  <Equipment type="cloak">

  Cape Elfique de discrétion
  *Discrétion +5 si utilisée avec les bottes*

  </Equipment>

  <Equipment type="ring">

  Anneau de défense DEF+2

  </Equipment>

  <Equipment type="gloves">-</Equipment>

  <Equipment type="belt">-</Equipment>

  <Equipment type="boots">

  Bottes Elfiques de discrétion
  *Discrétion +5 si utilisées avec la cape*

  </Equipment>
</Grid>
