---
aside: false
---

# Anathazerïn - Chroniques Oubliées Fantasy

## Mirielle

- Nom complet : Mìrielle Fénaril
- Race : Elfe Sylvain
- Classe : Druide
- Âge : 168 ans
- Taille : 1m67, 58kg

Fille de Tenruil, chef du village Thuléa dans la forêt de Hautesylve, Mìrielle a longtemps étudié les arts druidiques sur ses terres natales. Depuis plusieurs années, elle continue sa formation sur les terres de l'ouest et s’entretient régulièrement avec Maëla, la sorcière du Bois de Myrviel.

Calme et plutôt réfléchie, ses différentes aventures l'ont rendue tolérante vis à vis des autres races, notamment des nains. Cependant, sa fidèle panthère Enya, ne semble toujours pas accepter ces personnages court sur pattes...

Après de longues années de recherche dans différentes forêts de l'ouest, Mìrielle souhaite profiter de la grande cérémonie de présentation du Temple de Karoom, le Héros Nain de Clairval, pour s'enquérir de Maëla, n'ayant pas eu de ses nouvelles depuis un bon moment.

## Epilogue

Accompagné d'Athelstan, fier prêtre Drakéïde, de Silifrey dont les flèches pénètrent aussi bien la chair orc que les écailles d'Athelstan, et de Théogène, jamais à court d'idées pour inventer un plan infaillible, Mirielle a parcouru les Terres d'Osgild pendant de longs mois. La cité d'Anathazerïn a été découverte et détruite et l'anneau des Rois a été récupéré et rendu à la Rennes des Elfes.

Mais Mirielle n'a pu retrouver sa famille, probablement tuée par les orcs et les géants lors de l'invasion de la région de Thuléa. Maintenant que la Princesse Arraignée a été vaincu et que les armées ennemies ont été repoussées dans les montagnes du nord, le temps est à la reconstruction et à la paix. Pour le moment...
