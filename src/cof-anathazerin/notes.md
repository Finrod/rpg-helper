---
title: Infos utiles
prev: false
next: false
---

# Infos utiles

- Durée d'un tour : 10 secondes

## Montée de niveau

- Niveau +1
- 1 DV + mod. constitution (/r 1d8 +2) > ajouter aux PV max
- Reset des PV restants
- Reset des PC et PM
- Assigner 2 points de compétence
- Niveau +1 pour Enya
- PV Enya (niveau Mirielle x4)

## Règles de magie avancées

Pour 1 PM :
- Lancer le sort sur quelqu'un d'autre

Pour 1 PM de plus :
- Lancer le sort en 1 action d'attaque
- Doubler la portée
- Doubler la durée
- Ajouter un dé supplémentaire aux dégâts ou soin

Limites :
- Le cout total du sort ne peut dépasser son rang
- Le lanceur ne peut dépenser plus de PM à chaque tour que son niveau


## Points de chance

En plus d'ajouter 10 à votre score, vous pouvez utiliser vos points de chance pour :

### Réussite critique
Si un test était déjà une réussite simple, elle devient une réussite critique. Pour accélérer les combats, considérez que lorsqu’un personnage obtient une réussite critique en attaque contre un PNJ mineur, celui-ci est hors combat (mort, inconscient, assommé, en fuite, etc.). Il n’est pas nécessaire de lancer les DM. Cela revient effectivement à considérer qu’un PJ peut se débarrasser de tout PNJ mineur en dépensant un point de Chance.

### Poussée d’adrénaline
Un point de Chance (PC) permet d’obtenir une action supplémentaire (attaque simple ou action de mouvement) à son Initiative +10. Le personnage prendra ensuite son tour normal à son Initiative habituelle.

### Réussite héroïque
Lorsqu’un personnage obtient une réussite critique, il peut la transformer en réussite héroïque au prix de 1 PC. Il n’est pas possible de changer une réussite normale en réussite critique, puis en réussite héroïque. Avec une réussite héroïque, l’effet va au-delà de toutes les espérances. En combat, l’attaquant élimine 1d6 PNJ mineurs au contact ou à portée ou un seul adversaire majeur (il tombe à 0 PV). Hors combat, le personnage obtient bien plus que ce qui était initialement prévu, le MJ décide des effets exacts après avoir consulté le joueur sur ses souhaits.

### Tromper la mort
Un point de chance permet de garder un personnage en vie alors même qu’il aurait dû mourir. A la place, il est inconscient et récupérera 1 PV au bout de 1d6 jours de repos. A charge du MJ de trouver une explication plausible pour ce miracle ! En contrepartie, le PC utilisé pour tromper la mort est définitivement perdu.

## Liens utiles

- [DRS Chroniques Oubliées](https://www.co-drs.org/fr)
- [Bazar](https://cofbazar.github.io/index.html)
- [Règles (PC du MJ)](https://onedrive.live.com/redir?resid=CD487B2561F8CBA9%211414&authkey=%21Aj_A-X4h65zwKF4&page=View&wd=target%28R%C3%A8gles.one%7Ca2c21bec-318e-49b3-aab0-2bc259125c9f%2FVoies%7Cf8524664-3d98-4b01-9b5a-831188f0d3dc%2F%29)

## Détails des bonus

- Initiative
  - Voie du fauve : +1 x 4 rangs
  - Voie de l'Elfe Sylvain : +5
  - Voie de l'archer : mod de SAG (+6)

- Dextérité
  - Voie de l'Elfe Sylvain : +5

### Compétences

- Acrobatie
  - Dextérité : +5

- Athlétisme
  - Capacité de base : +3
  - Voie du fauve : +1 x 4 rangs

- Crochetage
  - Dextérité : +5

- Discrétion
  - Dextérité : +5
  - Voie de la nature : +2 x 2 rangs (en milieu naturel)
  - Bottes et cape Elfique de discrétion : +5

- Perception
  - Voie de l'archer : +2 x 5 rangs (voie de l'Elfe Sylvain)

- Survie
  - Voie de la nature : +2 x 2 rangs (en milieu naturel)
