---
title: Dragann
layout: page
prev: false
next: false
---

<script setup>
import stats from './stats.json'

import Grid from '@/Grid.vue'
import Stats from '@/Stats.vue'
import GridBox from '@/GridBox.vue'
import GridCol from '@/GridCol.vue'
import Equipment from '@/Equipment.vue'
import CapacityCounter from '@/CapacityCounter.vue'
</script>

# Dragann

<Grid>
  <GridCol :size="1">

  ## Aptitudes

  **Langues** : [Nain](https://www.aidedd.org/regles/races/nain/), Commun, Elfique, Gnome

  **[Serment de rédemption](https://www.aidedd.org/dnd-5/unearthed-arcana/serments-sacres/#redemption)** : Paix, Innocence, Patience, Sagesse

  **Style de combat** : Défense (+1 à la CA avec une armure)

  </GridCol>

  <GridCol :size="1">

  ## Dons

  **Vision dans le noir** : 18 mètres

  **Santé divine** : Immunisé contre les maladies

  **Résistance au poison** : Avantage aux JdS contre les poisons

  </GridCol>

  <GridCol :size="1">

  ## Emplacements de sorts

  <CapacityCounter char="dragann" id="inspiration" label="Inspiration" :start="0" unit="" simple-counter preventGlobalReset />
  <CapacityCounter char="dragann" id="spells-lvl-1" label="Niveau 1" :start="stats.spells.level1" unit="" reset-to-start />
  <CapacityCounter char="dragann" id="spells-lvl-2" label="Niveau 2" :start="stats.spells.level2" unit="" reset-to-start />
  <CapacityCounter char="dragann" id="spells-lvl-3" label="Niveau 3" :start="stats.spells.level3" unit="" reset-to-start />
  <CapacityCounter char="dragann" id="spells-lvl-4" label="Niveau 4" :start="stats.spells.level4" unit="" reset-to-start />

  </GridCol>

  <GridCol :size="1">
    <Stats :stats="stats" withLongRest />
  </GridCol>
</Grid>

## Compétences

<Grid>
  <GridBox type="paladin-spells">

  ### Sorts

  <CapacityCounter :label="`Nombre de sorts préparables : ${stats.mods.cha + Math.floor(stats.level / 2)}`" only-label />

  #### Sorts de serment

  - [Sanctuaire (niv 1)](https://www.aidedd.org/dnd/sorts.php?vf=sanctuaire) (action bonus)
  - [Sommeil (niv 1)](https://www.aidedd.org/dnd/sorts.php?vf=sommeil) (action)
  - [Contresort (niv 3)](https://www.aidedd.org/dnd/sorts.php?vf=contresort) (réaction | 18m)
  - [Motif hypnotique (niv 3)](https://www.aidedd.org/dnd/sorts.php?vf=motif-hypnotique) (action | conc.)
  - [Peau de pierre (niv 4)](https://www.aidedd.org/dnd/sorts.php?vf=peau-de-pierre) (action | conc.)
  - [Sphère résiliente d'Otiluke (niv 4)](https://www.aidedd.org/dnd/sorts.php?vf=sphere-resiliente-d-otiluke) (action | conc.)

  #### Sorts de niveau 1

  - [Bénédiction](https://www.aidedd.org/dnd/sorts.php?vf=benediction) (action | conc.)
  - [Bouclier de la foi](https://www.aidedd.org/dnd/sorts.php?vf=bouclier-de-la-foi) (action bonus | conc.)

  #### Sorts de niveau 2

  - [Restauration partielle](https://www.aidedd.org/dnd/sorts.php?vf=restauration-partielle) (action)
  - [Zone de vérité](https://www.aidedd.org/dnd/sorts.php?vf=zone-de-verite) (action)

  #### Sorts de niveau 3

  - [Aura du croisé](https://www.aidedd.org/dnd/sorts.php?vf=aura-du-croise) (action | conc. | aura 9m)
  - [Aura de vitalité](https://www.aidedd.org/dnd/sorts.php?vf=aura-de-vitalite) (action | conc. | aura 9m)
  - [Dissipation de la magie](https://www.aidedd.org/dnd/sorts.php?vf=dissipation-de-la-magie) (action)
  - [Délivrance des malédictions](https://www.aidedd.org/dnd/sorts.php?vf=delivrance-des-maledictions) (action)

  </GridBox>
  <GridBox type="paladin-aura">

  ### Auras

  #### Aura de protection (3m, 9m au niveau 18)
  Si vous ou une créature alliée située autour de vous doit effectuer un **jet de sauvegarde**, la créature gagne un **bonus de +{{ stats.mods.cha }}** (MOD de Charisme, minimum +1). Vous devez être conscient pour accorder ce bonus.

  #### Aura du gardien (3m, 9m au niveau 18)
  Vous pouvez protéger les autres des dommages au prix de votre propre santé. Par une **réaction** lorsqu'une créature autour de vous subit des dégâts, vous pouvez **prendre par magie ces dégâts à la place de la créature**. Cette capacité ne transfère **aucun autre effet** qui pourrait accompagner les dégâts, et ces dégâts ne peuvent être **réduits en aucune façon**.

  #### Aura de courage (3m, 9m au niveau 18)
  Vous et toutes créatures amicales situées autour de vous **ne pouvez être effrayés tant que vous êtes conscient**.

  </GridBox>
  <GridBox type="lay-on-hands">

  ### Imposition des mains

  <CapacityCounter char="dragann" id="lay-on-hands" label="Réserve de PV" :start="stats.level * 5" unit="" reset-to-start />

  Votre toucher béni peut guérir les blessures.

  Vous possédez une **réserve de points de vie à soigner qui se récupère après chaque repos long**. Avec cette réserve, vous pouvez restaurer un nombre total de points de vie égal à votre niveau de paladin multiplié par 5. Au prix d'une **action**, vous pouvez toucher une créature et puiser dans votre réserve pour soigner autant de points de vie que vous le désirez, sans dépasser le nombre de points dans votre réserve bien entendu.

  Vous pouvez également dépenser **5 points de vie de votre réserve pour guérir la cible d'une maladie ou neutraliser un poison qui l'affecte**. Vous pouvez soigner plusieurs maladies et neutraliser plusieurs poisons avec une seule imposition des mains en dépensant les points de vie séparément pour chacun d'entre eux. L'imposition des mains n'a pas d'effet sur les morts-vivants et les artificiels.

  </GridBox>
  <GridBox type="divine-conduit">

  ### Conduit Divin

  <CapacityCounter char="dragann" id="divine-conduit" label="Utilisation" :start="1" unit="" reset-to-start />

  #### Émissaire de paix
  Vous pouvez utiliser votre Conduit divin pour augmenter votre présence grâce à la puissance divine. Par une **action bonus**, vous vous accordez un bonus de **+5 à votre prochain jet de Charisme (Persuasion)** dans les prochaines 10 minutes.

  #### Blâme du violent
  Vous pouvez utiliser votre Conduit divin pour réprimander ceux qui utilisent la violence. Par une **réaction** lorsqu'un **ennemi dans un rayon de 9 mètres autour de vous inflige des dégâts avec une attaque au corps à corps à une créature autre que vous-même**, vous forcez cet attaquant à faire un **jet de sauvegarde de Sagesse**. En cas d'échec, l'attaquant subit un nombre de dégâts radiants égal aux dégâts qu'il vient d'infliger. En cas de sauvegarde réussie, il ne prend que la moitié des dommages.

  </GridBox>
  <GridBox type="divine-sense">

  ### Sens Divin

  <CapacityCounter char="dragann" id="divine-sense" label="Utilisations" :start="stats.mods.cha + 1" unit="" reset-to-start />

  Une forte présence maléfique éveille vos sens, comme une odeur nocive, et un bien puissant fait résonner dans vos oreilles une musique céleste.

  Par une **action**, vous pouvez éveiller votre conscience pour détecter de telles forces. **Jusqu'à la fin de votre prochain tour, vous connaissez l'emplacement de toute créature céleste, fiélon ou mort-vivante dans un rayon de 18 mètres autour de vous, et qui ne se trouve pas derrière un abri total**. Vous connaissez le **type** (céleste, fiélon ou mort-vivant) et le **nombre** de tous les êtres dont vous sentez la présence, mais **pas leur identité** (le vampire comte Strahd von Zarovich, par exemple). Dans ce même rayon, vous détectez également la présence d'un **lieu** ou d'un **objet** qui a **été consacré ou profané**, comme avec le sort sanctification.

  Vous pouvez utiliser cette capacité un nombre de fois égal à 1 + votre modificateur de Charisme. Lorsque vous terminez un repos long, vous récupérez toutes les utilisations dépensées.

  </GridBox>
  <GridBox type="divine-punishment">

  ### Châtiment Divin amélioré

  <CapacityCounter label="Utilisation : emplacements de sorts" only-label />

  Quand vous touchez une créature avec une arme de corps à corps, vous pouvez **utiliser n'importe quel emplacement de sort** (de paladin ou autre) pour châtier cette créature et lui infliger des **dégâts radiants** supplémentaires. Les dégâts supplémentaires infligés sont de **3d8 pour un emplacement de sort de niveau 1, plus 1d8 supplémentaire pour chaque niveau de sort supérieur à 1**, jusqu'à un maximum de 5d8.

  Si la créature est un **mort-vivant ou un fiélon, les dégâts augmentent de 1d8**, jusqu'à un maximum de 6d8.

  </GridBox>
</Grid>

<Grid>
  <GridCol :size="1">

  ## Buffs et auras

  <GridBox type="transparent">

  <CapacityCounter char="dragann" id="ayako-haste" label="Hâte d'Ayako" :start="10" preventGlobalReset />
  Vitesse x2, CA +2, +1 action, avantage aux jets de sauvegarde de DEX, 1 tour pour rien à la fin du sort

  <CapacityCounter char="dragann" id="vitality" label="Aura de vitalité" :start="10" preventGlobalReset />
  [Action bonus] 2d6 de soin à une créature dans les 9 mètres

  <CapacityCounter char="dragann" id="croisé" label="Aura du croisé" :start="10" preventGlobalReset />
  Tout allié dans les 9 mètres inflige 1d4 dégâts radiants supplémentaires lorsqu'il touche avec une arme

  </GridBox>
  </GridCol>

  <GridCol :size="5">

  ## Equipement

  <Grid>


  <Equipment type="axe">

  Hache "Voeux de Dragann"
  *A chaque attaque, lancer 1d6 : [1] -5 PV - [2-5] +8 PV - [6] +10 PO*
  *Une fois par jour, frappe et entrave (DD17) un ennemi dans un bloc d'or brut*
  *Une fois par partie, nullifie les dégâts d'une attaque*

  </Equipment>

  <Equipment type="shield">

  Bouclier
  *CA +2*

  </Equipment>

  <Equipment type="amulet">

  Symbole sacré
  *Amulette offerte par le temple de Heaume lorsque Dragann fut nommé Paladin*

  </Equipment>

  <Equipment type="armor" heavy>

  Harnois
  *CA 18*

  </Equipment>

  <Equipment type="ring">

  Anneau de résistance au feu

  </Equipment>

  <Equipment type="ring">

  Bague de noble de Baldur's Gate
  *Offerte par Zara Portyr, Lieutenante du Poing Enflammé pour nos services*

  </Equipment>

  <Equipment type="mount">

  Brokk, un bouquetin de guerre des Montagnes Naines
  *Offerte par Zara Portyr, Lieutenante du Poing Enflammé pour nos services*

  </Equipment>

  </Grid>
  </GridCol>
</Grid>
