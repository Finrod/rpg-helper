---
aside: false
---

# Descente en Averne - Donjon et Dragons

## Dragann

- Nom complet : Dragann
- Race : Nain des montagnes
- Classe : Paladin
- Âge : 72 ans
- Taille : 1m52, 71kg
- Alignement : Loyal Neutre

Le père de Dragann, Faldin, était un grand forgeron du clan des Crânes Luisants, spécialiste des haches. Pour une raison inexpliquée, il forgea une hache exceptionnelle a un étranger, un certain Darmin Zodge, mercenaire du poing enflammé.
A cause de cet acte, contraire aux règles du clan, il fut banni avec sa famille.

C'est dans un temple de Heaume, dieu de la protection, qu'ils ont trouvé refuge et que Dragann a grandi.

Élevé sous les préceptes du temple et formé à la forge par son père, il a choisi de devenir Paladin, pour protéger sa famille.
Il nourrit secrètement le désir de laver le nom de son père pour réintégrer le clan des Crânes Luisants.
Sa première quête est de retrouver ce Darmin Zodge...
