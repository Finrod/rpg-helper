---
title: Infos utiles
prev: false
next: false
---

# Infos utiles

- Durée d'un tour : 6 secondes

## Montée de niveau

- Niveau +1
- PV : (1d10 ou 6) + mod CON + 2 (don [robuste](https://www.aidedd.org/dnd/dons.php?vf=robuste))
- Augmenter la réserve d'imposition des mains
- Ajouter un sort à la liste si le nombre de sorts préparables a augmenté (Mod CHA + niveau / 2)
- Voir la page du [Paladin](https://www.aidedd.org/regles/classes/paladin/) pour les capacités supplémentaires
- Voir la page du [Serment](https://www.aidedd.org/dnd-5/unearthed-arcana/serments-sacres/#redemption) pour les capacités supplémentaires

## Liens utiles

- [Aide DD](https://www.aidedd.org/)
  - [Sorts](https://www.aidedd.org/dnd-filters/sorts.php)
  - [Dons](https://www.aidedd.org/dnd-filters/dons.php)
    - Idées: Vigeur naine, Endurant, Chanceux
  - [Objets magiques](https://www.aidedd.org/dnd-filters/objets-magiques.php)
  - [Actions en combat](https://www.aidedd.org/regles/combat/#actions)

## Hâte d'Ayako

Durée : 1 minute (10 tours)

Choisissez une créature consentante visible dans la portée du sort. Jusqu'à la fin de la durée du sort, la vitesse de la cible est doublée, elle bénéficie d'un bonus de +2 à la CA, elle a un avantage à ses jets de sauvegarde de Dextérité, et elle obtient une action supplémentaire à chacun de ses tours. Cette action peut être utilisée pour Attaquer (une seule attaque avec une arme), Foncer, Se désengager, Se cacher ou Utiliser un objet.
Lorsque le sort prend fin, la cible ne peut plus bouger ou agir jusqu'à la fin de son prochain tour, car une vague de léthargie la submerge.
