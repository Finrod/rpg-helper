---
title: Cacophonir
layout: page
prev: false
next: false
---

<script setup>
import stats from './stats.json'

import Grid from '@/Grid.vue'
import Stats from '@/Stats.vue'
import GridBox from '@/GridBox.vue'
import GridCol from '@/GridCol.vue'
import Equipment from '@/Equipment.vue'
import CapacityCounter from '@/CapacityCounter.vue'
import SpellModal from '@/SpellModal.vue'
</script>

# Cacophonir

<Grid>
  <GridCol :size="1">

  ## Aptitudes

  **Langues** : Commun, Elfique, Gnome

  **[Domaine artistique](https://www.aidedd.org/regles/historiques/artiste/)** : Acteur, Musicien, Acrobate

  **Expertise** (maitrise x2) : Discretion, Représentation

  **[Collège](./college.md)** : Poésie Epique

  </GridCol>

  <GridCol :size="1">

  ## Dons

  **Vision dans le noir** : 18 mètres

  **[Ascendance féerique](https://www.aidedd.org/regles/races/demi-elfe/)** :
  - • Avantage aux JdS contre les effets de charme
  - • La magie ne peux pas m'endormir

  </GridCol>

  <GridCol :size="1">

  ## Emplacements de sorts

  <CapacityCounter char="cacophonir" id="inspiration" label="Inspiration" :start="0" unit="" simple-counter preventGlobalReset />
  <CapacityCounter char="cacophonir" id="spells-lvl-1" label="Niveau 1" :start="stats.spells.level1" unit="" reset-to-start />
  <CapacityCounter char="cacophonir" id="spells-lvl-2" label="Niveau 2" :start="stats.spells.level2" unit="" reset-to-start />

  </GridCol>

  <GridCol :size="1">
    <Stats :stats="stats" withLongRest />
  </GridCol>
</Grid>

## Compétences

<Grid>
  <GridBox type="bard-spells">

  ### Sorts

  <CapacityCounter :label="`Nombre de sorts connus : ${stats.spells.knownMinor} mineurs et ${stats.spells.known} majeurs`" only-label />

  #### Sorts mineurs

  - [Main de mage](https://www.aidedd.org/dnd/sorts.php?vf=main-de-mage) (action)
  - [Moquerie curelle](https://www.aidedd.org/dnd/sorts.php?vf=moquerie-cruelle) (action) <SpellModal type="mock" />

  #### Sorts de niveau 1

  - [Four rire de Tasha](https://www.aidedd.org/dnd/sorts.php?vf=fou-rire-de-tasha) (action | conc.) <SpellModal type="joke" />
  - [Mot de guérison](https://www.aidedd.org/dnd/sorts.php?vf=mot-de-guerison) (action bonus | soin 1d4 + {{stats.mods.cha}})
  - [Déguisement](https://www.aidedd.org/dnd/sorts.php?vf=deguisement) (action)
  - [Héroïsme](https://www.aidedd.org/dnd/sorts.php?vf=heroisme) (action | conc.)
  - [Feuille morte](https://www.aidedd.org/dnd/sorts.php?vf=feuille-morte) (1 réaction)
  <!-- - [Murmures dissonants](https://www.aidedd.org/dnd/sorts.php?vf=feuille-morte) (1 réaction) -->

  #### Sorts de niveau 2

  - [Déblocage](https://www.aidedd.org/dnd/sorts.php?vf=deblocage) (action)

  </GridBox>
  <GridBox type="bard-inspiration">

  ### Inspiration Bardique (d6)

  <CapacityCounter char="cacophonir" id="bardic-inspiration" label="Dés disponibles" :start="stats.mods.cha" unit="" reset-to-start />

  Vous pouvez **inspirer les autres en maniant les mots ou la musique**. Pour ce faire, utilisez une **action bonus à votre tour** pour choisir une créature autre que vous-même dans un rayon de **18 mètres** autour de vous et qui peut vous entendre. Cette créature gagne un dé d'Inspiration bardique (d6). Une fois dans les **10 minutes suivantes, la créature peut lancer le dé et ajouter le nombre obtenu** à un jet de caractéristique, d'attaque ou de sauvegarde qu'elle vient de faire. La créature **peut attendre de voir le résultat** de jet de caractéristique, d'attaque ou de sauvegarde avant de décider d'appliquer le dé d'Inspiration bardique, **mais elle doit se décider avant que le MD ne dise si le jet est un succès ou un échec**. Une fois le dé d'Inspiration bardique lancé, il est consommé. Une créature ne peut avoir qu'un seul dé d'Inspiration bardique à la fois.

  Vous pouvez utiliser cette capacité un nombre de fois égal à votre modificateur de Charisme (minimum 1). Vous regagnez vos dés d'Inspiration bardique après avoir terminé un **repos long**. Votre dé d'Inspiration bardique change lorsque vous atteignez certains niveaux dans cette classe. Le dé passe à un d8 au niveau 5, un d10 au niveau 10, et un d12 au niveau 15.

  </GridBox>
  <GridBox type="bard-poetry">

  ### Collège de Poésie Epique

  <CapacityCounter char="cacophonir" id="verses" label="Strophes" :start="0" unit="" simple-counter preventGlobalReset />

  Rang du poème : 0

  Strophes requises pour le rang suivant : 4

  </GridBox>
  <GridBox type="bard-public">

  ### A la demande du public

  Vous pouvez **toujours trouver un endroit où vous produire**, souvent dans une auberge ou une taverne, mais également dans un cirque, un théâtre ou même dans la cour d'un noble. Dans de tels endroits, **on vous offre gratuitement le gîte et le couvert** (d'un train de vie modeste ou confortable, selon le type d'établissement), **tant que vous faites une représentation chaque nuit**. De plus, votre représentation fait de vous une **figure locale**. Lorsque des étrangers vous reconnaissent dans une ville où vous avez déjà joué, ils se prennent d'amitié pour vous.

  ---

  ### Touche-à-tout

  Vous pouvez ajouter la moitié de votre bonus de maîtrise (arrondi au chiffre inférieur) à tout jet de caractéristique qui n'applique pas déjà votre bonus de maîtrise.

  ---

  ### Chant reposant (1d6)

  Vous pouvez utiliser de la musique ou une oraison apaisante lors d'un repos court pour aider à revitaliser vos alliés blessés. Si vous ou toutes créatures amies qui peuvent entendre votre représentation récupérez des points de vie à la fin du repos court en dépensant un ou plusieurs dés de vie, chacune de ces créatures récupère 1d6 points de vie supplémentaires.

  </GridBox>
</Grid>

## Equipement

<Grid>
  <Equipment type="rapier">

  Rapière
  *1d8 + mod DEX de dégats perforants*

  </Equipment>

  <Equipment type="flute">

  Une flute

  </Equipment>

  <Equipment type="clothes">

  Armure de cuir
  *CA 11*

  </Equipment>

  <Equipment type="disguise">

  Trois costumes et un kit de déguisement

  </Equipment>

  <Equipment type="map">

  Babiole
  *Une carte au trésor indéchiffrable*

  </Equipment>
</Grid>
