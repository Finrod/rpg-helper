---
aside: false
---

# L'Odyssée des Seigneurs Dragons - Donjon et Dragons

## Cacophonir

- Nom complet : Cacophonir Thaloran
- Race : Demi-Elfe
- Classe : Barde
- Âge : 25 ans
- Taille : 1m70, 60kg

### Personnalité

- Alignement : Loyal Bon
- Trait de personnalité : Personne ne reste en colère contre moi très longtemps, car je peux désamorcer toute tension
- Idéaux : Lorsque je fais une représentation, j'essaye de rendre le monde meilleur qu'il ne l'est
- Liens : Ma flute est mon bien le plus précieux, elle m'a été offerte par ma mère quand j'étais enfant.
C'est une flute toute simple, au détail près qu'elle est d'un blanc étincelant, probablement un bois noble. Je la garde avec moi tout le temps, persuadé qu'elle m'aidera à devenir un grand Barde.
Depuis que j'ai quitté la maison, j'ai remarqué deux petites runes Elfiques au dos de la flute, j'ai consulté des érudits et chercher à les déchiffrer, mais personne n'a pu me dire ce que c'était. On m'a dit que c'était probablement un message d'amour de ma mère, mais je suis sûr que c'est un enchantement magique qui m'aidera à devenir un grand Barde !
- Défauts : Je ferais n'importe quoi pour gagner gloire et notoriété, mais je sais que je pourrai pas le faire seul

### Apparence

Cacophinir ne sort pas vraiment du lot. Il n'est pas très grand, pas vraiment costaud et habillé de manière simple. Il se balade avec peu de biens, mais a toujours un instrument de musique dans les mains.

### Histoire

Fils de Harmonia une elfe musicienne et de Jazz un père humain musicien, Cacophonir a grandi entouré d'arts et de mélodies, dans un petit village niché au cœur des bois elfiques, là où la musique et la magie se mêlent harmonieusement.

Pourtant, dès son plus jeune âge, il a montré un talent douteux pour la musique. Ses tentatives de chanter étaient souvent interrompues par des rires et des moqueries, même de la part de ses amis les plus proches.

Malgré tout, Cacophonir restait passionné par la musique. Il rêvait de devenir un barde légendaire. Inspiré par des contes sur des bardes épiques et des instruments enchantés, il décida de partir à l'aventure pour trouver LA voix et LES instruments ultimes qui feraient de lui le plus grand des bardes.

Alors qu'il parcourait les bois de son enfance, un étranger lui donna une mystérieuse carte pour se rendre sur une île iconnue : Thyléa.

Il rechercha des informations sur cette île pendant des mois, sans succès, et fini par prendre son courage en main et suivre la carte. Après un voyage incroyablement mouvementé, il accosta sur l'île mystérieuse et découvrit qu'une Oracle avait prédit son arrivée et l'attendait...
