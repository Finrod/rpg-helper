---
title: Infos utiles
prev: false
next: false
---

# Infos utiles

- Durée d'un tour : 6 secondes

## Maitrises

- Armures : armures légères
- Armes : armes courantes, arbalète de poing, épée longue, épée courte, rapière
- Outils : flute, luth, cornemuse, tambour, kit de déguisement

## Montée de niveau

- Niveau +1
- PV : (1d8 ou 5) + mod CON
- Ajouter un sort à la liste si le nombre de sorts connus a augmenté
- Voir la page du [Barde](https://www.aidedd.org/regles/classes/barde/) pour les capacités supplémentaires
- Voir la page du [Collège](./college.md) pour les capacités supplémentaires
- Changer le dé d'inspiration bardique aux niveaux 5, 10 et 15
- Changer les dégats de moquerie cruelle aux niveaux 5, 11 et 17
- Changer les soins de Chant reposant aux niveaux 9, 13 et 17

## Liens utiles

- [Aide DD](https://www.aidedd.org/)
  - [Sorts](https://www.aidedd.org/dnd-filters/sorts.php)
  - [Dons](https://www.aidedd.org/dnd-filters/dons.php)
  - [Objets magiques](https://www.aidedd.org/dnd-filters/objets-magiques.php)
  - [Actions en combat](https://www.aidedd.org/regles/combat/#actions)
