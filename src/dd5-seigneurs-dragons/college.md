---
title: Collège de Poésie Epique
outline: [2, 4]
prev: false
next: false
---

# Collège de Poésie Epique

Les bardes qui rejoignent le collège de poésie épique travaillent à la création d’un chef-d’œuvre, dans la pure lignée des poètes épiques. Tous les grands récits de l’histoire ont été adaptés au public sous forme d’œuvres poétiques composées de main de maître. Ils se sont transmis de génération en génération grâce à la tradition orale et ont fini un jour par être recopiés sur des rouleaux de papyrus pour être conservés dans d’imposantes bibliothèques.

Les bardes qui désirent composer leurs propres épopées doivent consacrer leurs premières années à l’étude de la philosophie, de la versification et de la musique pour capturer la splendeur des exploits des mortels et des dieux grâce au langage lyrique. Cependant, le rhapsode ne peut pas passer toute sa vie à l’Académie. Il devra pister les conflits qui prennent des proportions démesurées, car les guerres dévastatrices donnent naissance à de grands héros, auteurs de faits d’armes incroyables.

Vous vous êtes donc préparé à vivre une existence mouvementée, à la poursuite de guerriers bravant des dangers impossibles. Vous avez appris à retranscrire fidèlement le déroulement des événements dans le chaos des combats. Vous savez que si votre plume dérape à un moment charnière, la beauté de l’instant pourrait s’évanouir à jamais dans les limbes de l’histoire.

## Composition des strophes

Quand vous rejoignez le collège de poésie épique au niveau 3, vous commencez la composition de votre épopée. Lorsqu’un événement décisif se déroule au cours de vos voyages ou des combats auxquels vous assistez, vous pouvez utiliser votre réaction pour improviser une nouvelle strophe. Par événement décisif, il faut entendre les hasards du destin dont vous et vos alliés faites l’objet (plus précisément, des épisodes qu’il est impossible de revivre à l’identique). Vous devez être témoin des événements pour poursuivre votre poème.

- **Comédie** : un personnage obtient un 1 naturel sur un jet d’attaque ou de sauvegarde.
- **Hubris** : un personnage obtient un 20 naturel sur un jet d’attaque ou de sauvegarde.
- **Ironie** : un personnage rate un jet de sauvegarde après avoir gagné un dé d’Inspiration bardique.
- **Tragédie** : un ennemi réduit un personnage à 0 point de vie.

De plus, toutes les aventures trépidantes qui méritent selon vous d’être évoquées dans votre poème sont éligibles tant que le MJ vous donne son accord — mais ne poussez pas le bouchon trop loin.

Chaque fois que vous composez une nouvelle strophe pour votre épopée, annoncez-le à voix haute. Si le MJ est d’accord, ajoutez 1 strophe à votre poème. Si vous le souhaitez, vous pouvez noter les circonstances et les causes de l’événement pour pouvoir vous relire plus tard, mais rien ne vous y oblige. Le rang de votre poème augmente au fur et à mesure que vous lui ajoutez des strophes.

> **Nombre de strophes et taille du groupe**
>
> Le nombre de strophes indiqué est prévu pour un groupe de six personnages joueurs. Si votre groupe comporte moins de membres, le nombre de strophes nécessaires par rang de l’épopée diminue. Multipliez le nombre de joueurs du groupe par le rang du poème pour connaître les valeurs à atteindre. Par exemple, s’il y a 3 joueurs dans le groupe, vous aurez besoin de 18 strophes (3 x 6) pour atteindre le rang 6.

### Rang de l’épopée et effet

| Rang du poème | Strophes requises | Amélioration d'Inspiration Bardique |
|---------------|-------------------|-------------------------------------|
| 1             | 4                 | Résultat minimum: 2
| 2             | 8                 | Courage épique
| 3             | 12                | Résultat minimum: 3
| 4             | 16                | Clairvoyance épique
| 5             | 20                | Résultat minimum: 4
| 6             | 24                | Détermination épique
| 7             | 28                | Résultat minimum: 5
| 8             | 32                | Réflexes épiques
| 9             | 36                | Résultat minimum: 6
| 10            | 40                | Résistance épique

## Inspirations poétiques

À partir du niveau 3, quand un allié bénéficie de votre Inspiration bardique, vous récitez une partie de votre épopée, ce qui améliore les effets du dé en fonction du rang du poème.

**Jets améliorés** : Selon le nombre de strophes que vous avez composées, la valeur minimale de vos dés d’Inspiration bardique varie. Quand un personnage lance un dé d’Inspiration bardique, s’il obtient un résultat inférieur au minimum indiqué pour le rang de votre poème, on transforme le résultat du dé en la valeur indiquée par le rang de votre poème.

**Effets supplémentaires** : Vos dés d’Inspiration bardique produisent d’autres effets en fonction du nombre de strophes. Quand vous donnez un dé d’Inspiration bardique à un allié, choisissez un effet parmi ceux que vous avez débloqués. Votre allié bénéficie de cet effet tant qu’il conserve votre dé d’Inspiration bardique. Une fois le dé lancé, l’effet disparaît.

- **Courage épique** : vous êtes avantagés sur les jets de sauvegarde pour vous protéger des effets de terreur.
- **Détermination épique** : vous êtes avantagés sur les jets de sauvegarde contre la mort.
- **Clairvoyance épique** : On ne peut pas vous prendre par surprise, et votre Perception passive augmente de 5.
- **Réflexes épiques** : Vous êtes avantagé sur les jets de sauvegarde contre les sorts qui affectent plusieurs cibles.
- **Résistance épique** : Vous gagnez une résistance contre un type de dégâts (au choix du barde).

## Égide du poète

À partir du niveau 6, vous maîtrisez les armures moyennes, ce qui vous permet d’être au plus près des combats quand vous composez votre poème. En outre, si vous êtes à 1,5 m d’un allié lorsque vous composez une strophe le concernant, vous regagnez un dé d’Inspiration bardique.

## Épithètes protectrices

À partir du niveau 14, les épithètes lyriques dont vous affublez vos alliés gagnent des vertus magiques et les protègent du danger. Lorsqu’un personnage avec un dé d’Inspiration bardique devrait être réduit à 0 point de vie, il peut choisir de lancer votre dé d’Inspiration bardique et de perdre autant de points de vie que le résultat indiqué au lieu de mourir. Ensuite, le dé d’Inspiration bardique est perdu.

> **Assigner des épithètes**
>
> Si vous voulez faire fonctionner votre imagination, n’hésitez pas à donner une épithète à chacun de vos alliés. Les épithètes homériques sont des titres assez courts comme « aux joues roses », « à l’esprit inflexible » ou « aux yeux clairs ». Les rhapsodes y recourent pour insister sur les vertus de leurs personnages. Quand vous citez vos alliés en composant vos strophes, utilisez leurs épithètes : « Orion au visage austère abat son bras et manque son adversaire ». Soyez respectueux, et ne choisissez pas des qualificatifs qui mettent vos camarades mal à l’aise.
